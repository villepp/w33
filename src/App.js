// App.js
import React from 'react';
import './App.css';
import Bird from './Bird'; // make sure the path is correct

function App() {
  return (
    <div className="App">
      <Bird />
    </div>
  );
}

export default App;
