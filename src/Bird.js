import React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import birdData from "./bird.json";
const Bird = () => {
  const sortedBirdData = birdData.sort((a, b) =>
    a.finnish.localeCompare(b.finnish)
  );

  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="bird table">
        <TableHead style={{ backgroundColor: "lightgray" }}>
          <TableRow>
            <TableCell sx={{ fontWeight: "bold", textAlign: "left" }}>
              Finnish
            </TableCell>
            <TableCell sx={{ fontWeight: "bold", textAlign: "left" }}>
              Swedish
            </TableCell>
            <TableCell sx={{ fontWeight: "bold", textAlign: "left" }}>
              English
            </TableCell>
            <TableCell sx={{ fontWeight: "bold", textAlign: "left" }}>
              Short
            </TableCell>
            <TableCell sx={{ fontWeight: "bold", textAlign: "left" }}>
              Latin
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {sortedBirdData.map((bird) => (
            <TableRow
              key={bird.short}
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell align="left">{bird.finnish}</TableCell>
              <TableCell align="left">{bird.swedish}</TableCell>
              <TableCell align="left">{bird.english}</TableCell>
              <TableCell align="left">{bird.short}</TableCell>
              <TableCell align="left">{bird.latin}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default Bird;
